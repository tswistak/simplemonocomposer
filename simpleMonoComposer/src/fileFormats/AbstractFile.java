package fileFormats;

import java.io.File;

public abstract class AbstractFile implements FileFormat {
	
	protected File file;
	protected String text;
	
	/**
	 * Constructor of AbstractFile
	 * 
	 * @param sheet String with contents of sheet
	 * @param file
	 */
	public AbstractFile(String sheet, File file){
		this.file=file;
		text=sheet;
	}
	
	@Override
	public abstract boolean save();

}
