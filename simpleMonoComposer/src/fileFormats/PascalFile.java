package fileFormats;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

import utilities.Note;

public class PascalFile extends AbstractFile {
	
	public PascalFile(String sheet, File file) {
		super(sheet, file);
	}
	
	private String header(){
		return String.format("{ GENERATED IN simpleMonoComposer }%nuses crt;%n%nbegin%n");
	}
	
	private String convertLine(String line){
		Note note = new Note(line);
		if (note.isError())
			return "{ ERROR IN LINE }\n";
		// PC Speaker so instrument and volume are ignored (assuming volumes lower than 50 are silence)
		String commentLine = String.format("{ %s }",note.toString());
		String pitchSet = new String("");
		if (note.getVolume()>=50)
			pitchSet = String.format("sound(%d);", note.getFrequency());
		String durationSet = String.format("delay(%d);", note.getDuration());
		return String.format("%s%n%s%n%s%nnosound;%n", commentLine, pitchSet, durationSet);
	}
	
	private String footer(){
		return new String("end.");
	}

	@Override
	public boolean save() {
		try{
			PrintWriter writer = new PrintWriter(file);
			writer.println(header());
			Scanner scanner = new Scanner(text);
			while (scanner.hasNextLine())
				writer.println(convertLine(scanner.nextLine()));
			writer.println(footer());
			writer.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
