
package fileFormats;

public interface FileFormat {
	/**
	 * Saves file in specific format
	 * 
	 * @return true if was saved succesfully, false otherwise
	 */
	public boolean save();
}
