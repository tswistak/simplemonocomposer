package fileFormats;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

import utilities.Note;

public class MipsFile extends AbstractFile {
	
	public MipsFile(String sheet, File file){
		super(sheet,file);
	}
	
	private String header(){
		return String.format("# GENERATED IN simpleMonoComposer%n# WORKS ONLY IN MARS%n%nli $v0, 33%n");
	}
	
	private String convertLine(String line){
		Note note = new Note(line);
		if (note.isError())
			return "# ERROR IN LINE\n";
		String commentLine = String.format("# %s",note.toString());
		String pitchSet = String.format("li $a0, %d", note.getPitch());
		String durationSet = String.format("li $a1, %d", note.getDuration());
		String instrumentSet = String.format("li $a2, %d", note.getInstrument());
		String volumeSet = String.format("li $a3, %d", note.getVolume());
		return String.format("%s%s%n%s%n%s%n%s%nsyscall%n", commentLine, pitchSet, durationSet, instrumentSet, volumeSet);
	}
	
	@Override
	public boolean save() {
		try{
			PrintWriter writer = new PrintWriter(file);
			writer.println(header());
			Scanner scanner = new Scanner(text);
			while (scanner.hasNextLine())
				writer.println(convertLine(scanner.nextLine()));
			writer.close();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
