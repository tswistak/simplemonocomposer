
import java.awt.EventQueue;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiChannel;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Synthesizer;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JTextPane;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.JInternalFrame;
import javax.swing.JSplitPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JDialog;
import java.awt.Dimension;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSpinner;
import java.awt.Font;
import javax.swing.SpinnerNumberModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.filechooser.FileFilter;
import javax.swing.ScrollPaneConstants;
import javax.swing.JScrollBar;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import javax.swing.SpringLayout;

import fileFormats.MipsFile;
import fileFormats.PascalFile;

import utilities.Note;
import utilities.TextFileFilter;


public class MainWindow {
	
	private final String[] notes = new String[] {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
	private final String[] lengths = new String[] {"1", "1/2", "1/4", "1/8", "1/16", "1/32", "1/64", "1/128"};

	private JFrame frmSimplemonocomposer;
	private JTextArea sheet;
	private JSpinner bpmSpinner;
	private JSpinner instrumentSpinner;
	private JSpinner volumeSpinner;
	private JSpinner octaveSpinner;
	private JComboBox noteBox;
	private JComboBox lengthBox;
	
	private File activeFile=null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmSimplemonocomposer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSimplemonocomposer = new JFrame();
		frmSimplemonocomposer.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				quitAction();
			}
		});
		frmSimplemonocomposer.getContentPane().setFont(new Font("SansSerif", Font.PLAIN, 12));
		frmSimplemonocomposer.setTitle("simpleMonoComposer - Untitled");
		frmSimplemonocomposer.setBounds(100, 100, 554, 449);
		frmSimplemonocomposer.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);		
		
		JMenuBar menuBar = new JMenuBar();
		frmSimplemonocomposer.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mntmNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean possible=true;
				if (!getSheet().getText().equals("")){
					int returnVal=JOptionPane.showOptionDialog(frmSimplemonocomposer, 
							"This option will delete your not saved work.\nAre you sure?", 
							"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
					possible = returnVal==JOptionPane.YES_OPTION;
				}
				if (possible){
					frmSimplemonocomposer.setTitle("simpleMonoComposer - Untitled");
					activeFile = null;
					getSheet().setText("");
				}
			}
		});
		mntmNew.setIcon(null);
		mnFile.add(mntmNew);
		
		JMenuItem mntmLoad = new JMenuItem("Load");
		mntmLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				boolean possible=true;
				if (!getSheet().getText().equals("")){
					int returnVal=JOptionPane.showOptionDialog(frmSimplemonocomposer, 
							"This option will delete your not saved work.\nAre you sure?", 
							"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
					possible = returnVal==JOptionPane.YES_OPTION;
				}
				if (possible){
					JFileChooser fileChooser = new JFileChooser();
					fileChooser.setMultiSelectionEnabled(false);
					fileChooser.setFileFilter(new TextFileFilter());
					int returnVal = fileChooser.showOpenDialog(frmSimplemonocomposer);
					if (returnVal == JFileChooser.APPROVE_OPTION){
						activeFile = fileChooser.getSelectedFile();
						// the following line is taken from: https://weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner.html
						// better than using a loop to add every line from a file
						String text=new String();
						try {
							text = new Scanner(activeFile,"UTF-8").useDelimiter("\\A").next();
							frmSimplemonocomposer.setTitle(String.format("simpleMonoComposer - %s", activeFile.getName()));
						} catch (Exception e) {
							JOptionPane.showMessageDialog(frmSimplemonocomposer, "Error while loading file:\n"+e,"Error!",JOptionPane.ERROR_MESSAGE);
							e.printStackTrace();
						}
						getSheet().setText(text);
					}
				}
			}
		});
		mntmLoad.setIcon(null);
		mnFile.add(mntmLoad);
		
		JMenuItem mntmSave_1 = new JMenuItem("Save");
		mntmSave_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (activeFile!=null)
					saveAction();
				else
					saveAsAction();
			}
		});
		mnFile.add(mntmSave_1);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setIcon(null);
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				quitAction();
			}
		});
		
		JMenuItem mntmSave = new JMenuItem("Save as...");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveAsAction();
			}
		});
		mntmSave.setIcon(null);
		mnFile.add(mntmSave);
		mnFile.add(mntmExit);
		
		JMenu mnExport = new JMenu("Export to...");
		menuBar.add(mnExport);
		
		JMenuItem mntmMipsCodemars = new JMenuItem("MIPS code (MARS)");
		mntmMipsCodemars.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser(){
					@Override
					public void approveSelection(){
						File file = getSelectedFile();
				        if(file.exists()){
				            int result = JOptionPane.showConfirmDialog(this,"Selected file will be overwritten. Are you sure?","File exists",JOptionPane.YES_NO_CANCEL_OPTION);
				            switch(result){
				                case JOptionPane.YES_OPTION:
				                    super.approveSelection();
				                    return;
				                case JOptionPane.NO_OPTION:
				                    return;
				                case JOptionPane.CLOSED_OPTION:
				                    return;
				                case JOptionPane.CANCEL_OPTION:
				                    cancelSelection();
				                    return;
				            }
				        }
				        super.approveSelection();
					}
				};
				fileChooser.setMultiSelectionEnabled(false);
				int returnVal = fileChooser.showSaveDialog(frmSimplemonocomposer);
				if (returnVal == JFileChooser.APPROVE_OPTION){
					File file = fileChooser.getSelectedFile();
					MipsFile export = new MipsFile(getSheet().getText(), file);
					boolean save = export.save();
					if (save)
						JOptionPane.showMessageDialog(frmSimplemonocomposer, "File exported succesfully!","Success",JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(frmSimplemonocomposer, "Error while saving file!","Error!",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mntmMipsCodemars.setIcon(null);
		mnExport.add(mntmMipsCodemars);
		
		JMenuItem mntmTurboPascalCode = new JMenuItem("Turbo Pascal code (PC Speaker)");
		mntmTurboPascalCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fileChooser = new JFileChooser(){
					@Override
					public void approveSelection(){
						File file = getSelectedFile();
				        if(file.exists()){
				            int result = JOptionPane.showConfirmDialog(this,"Selected file will be overwritten. Are you sure?","File exists",JOptionPane.YES_NO_CANCEL_OPTION);
				            switch(result){
				                case JOptionPane.YES_OPTION:
				                    super.approveSelection();
				                    return;
				                case JOptionPane.NO_OPTION:
				                    return;
				                case JOptionPane.CLOSED_OPTION:
				                    return;
				                case JOptionPane.CANCEL_OPTION:
				                    cancelSelection();
				                    return;
				            }
				        }
				        super.approveSelection();
					}
				};
				fileChooser.setMultiSelectionEnabled(false);
				int returnVal = fileChooser.showSaveDialog(frmSimplemonocomposer);
				if (returnVal == JFileChooser.APPROVE_OPTION){
					File file = fileChooser.getSelectedFile();
					PascalFile export = new PascalFile(getSheet().getText(), file);
					boolean save = export.save();
					if (save)
						JOptionPane.showMessageDialog(frmSimplemonocomposer, "File exported succesfully!","Success",JOptionPane.INFORMATION_MESSAGE);
					else
						JOptionPane.showMessageDialog(frmSimplemonocomposer, "Error while saving file!","Error!",JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mntmTurboPascalCode.setIcon(null);
		mnExport.add(mntmTurboPascalCode);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);
		toolBar.setOrientation(SwingConstants.VERTICAL);

		frmSimplemonocomposer.getContentPane().add(toolBar, BorderLayout.NORTH);
		
		Box basicConfig = Box.createHorizontalBox();
		toolBar.add(basicConfig);
		
		JLabel lblNewLabel_1 = new JLabel("BPM:");
		basicConfig.add(lblNewLabel_1);
		
		bpmSpinner = new JSpinner();
		bpmSpinner.setToolTipText("BPM as length of quater note");
		basicConfig.add(bpmSpinner);
		bpmSpinner.setModel(new SpinnerNumberModel(120, 40, 208, 1));
		
		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		basicConfig.add(horizontalStrut_3);
		
		JLabel lblInstrument = new JLabel("Instrument:");
		basicConfig.add(lblInstrument);
		
		instrumentSpinner = new JSpinner();
		instrumentSpinner.setToolTipText("Instruments (0-127 as in MIDI standard)");
		basicConfig.add(instrumentSpinner);
		instrumentSpinner.setModel(new SpinnerNumberModel(0, 0, 127, 1));
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		basicConfig.add(horizontalStrut_2);
		
		JLabel lblVolume = new JLabel("Volume:");
		basicConfig.add(lblVolume);
		
		volumeSpinner = new JSpinner();
		basicConfig.add(volumeSpinner);
		volumeSpinner.setToolTipText("Volume (0-127)");
		volumeSpinner.setModel(new SpinnerNumberModel(100, 0, 127, 1));
		
		Box noteConfig = Box.createHorizontalBox();
		toolBar.add(noteConfig);
		
		JLabel lblNewLabel = new JLabel("Octave #:");
		noteConfig.add(lblNewLabel);
		
		octaveSpinner = new JSpinner();
		octaveSpinner.setToolTipText("Octave #");
		octaveSpinner.setModel(new SpinnerNumberModel(5, 0, 10, 1));
		noteConfig.add(octaveSpinner);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		noteConfig.add(horizontalStrut);
		
		JLabel lblNote = new JLabel("Note:");
		noteConfig.add(lblNote);
		
		noteBox = new JComboBox();
		noteBox.setModel(new DefaultComboBoxModel(notes));
		noteBox.setToolTipText("Note selection");
		noteConfig.add(noteBox);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		noteConfig.add(horizontalStrut_1);
		
		JLabel lblNoteLength = new JLabel("Note length:");
		noteConfig.add(lblNoteLength);
		
		lengthBox = new JComboBox();
		lengthBox.setToolTipText("Note length");
		lengthBox.setModel(new DefaultComboBoxModel(lengths));
		lengthBox.setSelectedIndex(2);
		noteConfig.add(lengthBox);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int noteIndex = getNoteBox().getSelectedIndex();
				int octave = (int) getOctaveSpinner().getValue();
				int lengthIndex = getLengthBox().getSelectedIndex();
				int bpm = (int) getBpmSpinner().getValue();
				int instrument = (int) getInstrumentSpinner().getValue();
				int volume = (int) getVolumeSpinner().getValue();
				Note tmp = new Note(notes[noteIndex],octave,lengths[lengthIndex],bpm,instrument,volume);
				getSheet().append(tmp.toString());
			}
		});
		noteConfig.add(btnNewButton);
		
		JButton btnPreview = new JButton("Preview");
		btnPreview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int noteIndex = getNoteBox().getSelectedIndex();
				int octave = (int) getOctaveSpinner().getValue();
				int lengthIndex = getLengthBox().getSelectedIndex();
				int bpm = (int) getBpmSpinner().getValue();
				int instrument = (int) getInstrumentSpinner().getValue();
				int volume = (int) getVolumeSpinner().getValue();
				Note tmp = new Note(notes[noteIndex],octave,lengths[lengthIndex],bpm,instrument,volume);
				if (!tmp.isError()){
					try{
						Synthesizer synth = MidiSystem.getSynthesizer(); // default system synthesizer
						// short message for our actions
						ShortMessage setInstrument = new ShortMessage();
						ShortMessage playNote = new ShortMessage();
						ShortMessage stopNote = new ShortMessage();
						synth.open();
						Receiver receiver = synth.getReceiver(); // default receiver
						setInstrument.setMessage(ShortMessage.PROGRAM_CHANGE,0,tmp.getInstrument(),0);
						playNote.setMessage(ShortMessage.NOTE_ON,0,tmp.getPitch(),tmp.getVolume());
						stopNote.setMessage(ShortMessage.NOTE_OFF,0,tmp.getPitch());
						receiver.send(setInstrument, -1);
						receiver.send(playNote, -1);
						try {
							Thread.sleep(tmp.getDuration());
						} catch (InterruptedException e) {
							e.printStackTrace();
						} finally {
							receiver.send(stopNote, -1);
						}
						synth.close();
					} catch (MidiUnavailableException | InvalidMidiDataException e){
						e.printStackTrace();
						JOptionPane.showMessageDialog(frmSimplemonocomposer, "MIDI error. Couldn't generate preview!","Error!",JOptionPane.ERROR_MESSAGE);
					}
				}
				else
					JOptionPane.showMessageDialog(frmSimplemonocomposer, "Wrong data. Couldn't generate preview!","Error!",JOptionPane.ERROR_MESSAGE);
			}
		});
		noteConfig.add(btnPreview);
		
		JButton btnPlay = new JButton("Play");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!getSheet().getText().equals("")){
					try{
						Synthesizer synth = MidiSystem.getSynthesizer(); // default system synthesizer
						// short message for our actions
						ShortMessage setInstrument = new ShortMessage();
						ShortMessage playNote = new ShortMessage();
						ShortMessage stopNote = new ShortMessage();
						Note tmp;
						synth.open();
						Receiver receiver = synth.getReceiver(); // default receiver
						Scanner scanner = new Scanner(getSheet().getText());
						while (scanner.hasNextLine()){
							tmp= new Note(scanner.nextLine());
							if (!tmp.isError()){
								setInstrument.setMessage(ShortMessage.PROGRAM_CHANGE,0,tmp.getInstrument(),0);
								playNote.setMessage(ShortMessage.NOTE_ON,0,tmp.getPitch(),tmp.getVolume());
								stopNote.setMessage(ShortMessage.NOTE_OFF,0,tmp.getPitch());
								receiver.send(setInstrument, -1);
								receiver.send(playNote, -1);
								try {
									Thread.sleep(tmp.getDuration());
								} catch (InterruptedException e) {
									e.printStackTrace();
								} finally {
									receiver.send(stopNote, -1);
								}
							}
						}
						synth.close();
					} catch (MidiUnavailableException | InvalidMidiDataException e){
						e.printStackTrace();
						JOptionPane.showMessageDialog(frmSimplemonocomposer, "MIDI error. Couldn't generate preview!","Error!",JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		noteConfig.add(btnPlay);
		
		sheet = new JTextArea();
		sheet.setWrapStyleWord(true);
		sheet.setLineWrap(true);
		JScrollPane scrollPane = new JScrollPane(sheet);
		frmSimplemonocomposer.getContentPane().add(scrollPane);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
	
	}

	public JTextArea getSheet() {
		return sheet;
	}
	public JSpinner getBpmSpinner() {
		return bpmSpinner;
	}
	public JSpinner getInstrumentSpinner() {
		return instrumentSpinner;
	}
	public JSpinner getVolumeSpinner() {
		return volumeSpinner;
	}
	public JSpinner getOctaveSpinner() {
		return octaveSpinner;
	}
	public JComboBox getNoteBox() {
		return noteBox;
	}
	public JComboBox getLengthBox() {
		return lengthBox;
	}
	
	public void quitAction(){
		boolean possible=true;
		if (!getSheet().getText().equals("")){
			int returnVal = JOptionPane.showOptionDialog(frmSimplemonocomposer, 
					"This option will delete your not saved work.\nAre you sure?", 
					"Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, null, null);
			possible = returnVal==JOptionPane.YES_OPTION;
		}
		if (possible)
			frmSimplemonocomposer.dispose();
	}
	
	public void saveAction(){
		try {
			PrintWriter writer = new PrintWriter(activeFile);
			writer.print(getSheet().getText());
			writer.close();
			frmSimplemonocomposer.setTitle(String.format("simpleMonoComposer - %s", activeFile.getName()));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(frmSimplemonocomposer, "Error while saving file:\n"+e,"Error!",JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}
	
	public void saveAsAction(){
		// overriding approveSelection() for overwrite check - solution found on stackoverflow
		JFileChooser fileChooser = new JFileChooser(){
			@Override
			public void approveSelection(){
				File file = getSelectedFile();
		        if(file.exists()){
		            int result = JOptionPane.showConfirmDialog(this,"Selected file will be overwritten. Are you sure?","File exists",JOptionPane.YES_NO_CANCEL_OPTION);
		            switch(result){
		                case JOptionPane.YES_OPTION:
		                    super.approveSelection();
		                    return;
		                case JOptionPane.NO_OPTION:
		                    return;
		                case JOptionPane.CLOSED_OPTION:
		                    return;
		                case JOptionPane.CANCEL_OPTION:
		                    cancelSelection();
		                    return;
		            }
		        }
		        super.approveSelection();
			}
		};
		fileChooser.setMultiSelectionEnabled(false);
		fileChooser.setFileFilter(new TextFileFilter());
		int returnVal = fileChooser.showSaveDialog(frmSimplemonocomposer);
		if (returnVal == JFileChooser.APPROVE_OPTION){
			if(!fileChooser.getSelectedFile().getAbsolutePath().endsWith("txt") && !fileChooser.getSelectedFile().getAbsolutePath().endsWith("csv"))
			    activeFile = new File(fileChooser.getSelectedFile() + ".txt");
			else
				activeFile = fileChooser.getSelectedFile();
			saveAction();
		}
	}
}
