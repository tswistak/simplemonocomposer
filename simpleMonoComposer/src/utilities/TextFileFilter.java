package utilities;

import java.io.File;

import javax.swing.filechooser.FileFilter;


public class TextFileFilter extends FileFilter {

	@Override
	public boolean accept(File f) {
		return f.isFile() ? f.getName().endsWith(".txt") || f.getName().endsWith(".csv") : true;
	}

	@Override
	public String getDescription() {
		return new String("*.txt, *.csv");
	}

}
