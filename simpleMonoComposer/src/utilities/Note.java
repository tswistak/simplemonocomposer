package utilities;
import java.util.StringTokenizer;


public class Note {

	private int pitch; // 0-127 as in MIDI
	private int duration; // in milliseconds (1 s = 1000 ms)
	private int instrument; // 0-127 as in MIDI
	private int volume; // 0-127 as in MIDI
	private boolean error=false;
	
	/**
	 * Creates note object
	 * 
	 * @param note String representation of sound
	 * @param octave Represents in which octave is given sound (0-10)
	 * @param noteLength Length of note represented as string containing a fraction
	 * @param BPM Tempo in which note is played
	 * @param instrument 0-127 as in MIDI
	 * @param volume 0-127 as in MIDI
	 */
	public Note(String note, int octave, String noteLength, int bpm, int instrument, int volume){
		// following two doesn't require any changes
		this.volume=volume;
		this.instrument=instrument;
		// calculating pitch
		pitch=12*octave+convertNote(note); // 12 sounds in each octave
		error = pitch>127 || pitch<0; // pitch can't be over 127 or lower than 0
		// calculating duration
		// 1 BPM = 1/60 Hz
		double tmp = 1/((double)bpm/60)*1000; // convert BPM to Hz (bpm/60) -> convert Hz to s (1/Hz) -> convert s to ms -> s*1000
		tmp*=convertNoteLength(noteLength); // multiplying by note length (multiplied earlier by 4 for convenience)
		duration = (int) Math.round(tmp); // double to int
		if (!error)
			error = duration<0; // duration cannot be negative, checked only if error was previously set on false
	}
	
	/**
	 * Creates note object from a string
	 * 
	 * @param note String in a format "pitch,duration,instrument,volume"
	 */
	public Note(String note){
		StringTokenizer token = new StringTokenizer(note,",");
		// Tokens: pitch, duration, instrument, volume
		try {
			String tmp=token.nextToken(); // pitch
			this.pitch=Integer.parseInt(tmp);
			error=pitch<0 || pitch>127;

			tmp=token.nextToken(); // duration
			this.duration=Integer.parseInt(tmp);
			if (!error) error=duration<0;

			tmp=token.nextToken(); // instrument
			this.instrument=Integer.parseInt(tmp);
			if (!error) error=instrument<0 || instrument>127;

			tmp=token.nextToken(); // volume
			this.volume=Integer.parseInt(tmp);
			if (!error) error=volume<0 || volume>127;
		} catch(Exception e) {
			error=true;
		}
	}
	
	private int convertNote(String note){
		// instead of 12 if's or switch cases - linear search in table
		String[] notes = new String[] {"C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"};
		// in this way, we have table index the same as index of note we need
		boolean found = false;
		int i; // declared before loop for later usage
		for (i=0; i<notes.length && !found; i++){
			found=note.equals(notes[i]);
		}
		return found ? i : -1; // -1 so pitch will be calculated lower than 0 and return error
	}
	
	private double convertNoteLength(String noteLength){
		// because we count BPM as a length of quarternote, returned values are multiplied by 4
		switch (noteLength){
		case "1": return 4;
		case "1/2": return 2;
		case "1/4": return 1;
		case "1/8": return 0.5;
		case "1/16": return 0.25;
		case "1/32": return 0.125;
		case "1/64": return 0.0625;
		case "1/128": return 0.03125;
		default: return -1; // -1 so it will generate error
		}
	}
	
	private int convertPitchToFrequency(int pitch){
		// A4 on piano - 49; in MIDI - 69
		// equation from http://en.wikipedia.org/wiki/Piano_key_frequencies
		double tmp;
		tmp=((double)pitch-69.0)/12;
		tmp=Math.pow(2,tmp)*440;
		return (int)Math.round(tmp);
	}
	
	
	/**
	 * @return pitch value, -1 if error
	 */
	public int getPitch(){
		return error ? -1 : pitch;
	}
	
	/**
	 * @return frequency (rounded) of pitch, -1 if error
	 */
	public int getFrequency(){
		return error ? -1 : convertPitchToFrequency(pitch);
	}
	
	/**
	 * @return duration in milliseconds, -1 if error
	 */
	public int getDuration(){
		return error ? -1 : duration;
	}
	
	/**
	 * @return midi instrument number, -1 if error
	 */
	public int getInstrument(){
		return error ? -1 : instrument;
	}
	
	/**
	 * @return volume of sound, -1 if error
	 */
	public int getVolume(){
		return error ? -1 : volume;
	}
	
	/**
	 * @return true if there was an error
	 */
	public boolean isError(){
		return error;
	}
	
	public String toString(){
		// if error is true we doesn't return anything
		// every correct line we end with line seperator
		return error ? "ERROR\n" : String.format("%d,%d,%d,%d%n", pitch, duration, instrument, volume);
		// this method will be used only for export to text file
	}
}
